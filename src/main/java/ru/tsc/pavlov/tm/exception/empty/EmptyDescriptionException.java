package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error: Description is empty.");
    }

    public EmptyDescriptionException(String value) {
        super("Error: Description of project" + value + " is empty.");
    }

}
