package ru.tsc.pavlov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.dto.Domain;
import ru.tsc.pavlov.tm.exception.entity.EntityNotFoundException;

public class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String BACKUP_XML = "./backup.xml";

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";

    @NotNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";

    @NotNull
    protected static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    @NotNull
    protected static final String JAXB_JSON_PROPERTY_VALUE = "application/json";

    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";


    @NotNull
    @SneakyThrows
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) throw new EntityNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new EntityNotFoundException();
        serviceLocator.getUserService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void execute() {

    }

}
