package ru.tsc.pavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand{

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.USER_CHANGE_PASSWORD;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Changing password for user";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSPHRASE:");
        @Nullable final String password = TerminalUtil.nextLine();

        @Nullable final User user = getUserService().findByLogin(login);
        user.setPassword(password);
        System.out.println("PASSPHRASE CHANGED");
        showUser(user);
    }

}
