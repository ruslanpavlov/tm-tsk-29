package ru.tsc.pavlov.tm.command.mutually;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.empty.EmptyIndexException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractMutuallyCommand {

    @Nullable
    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.PROJECT_REMOVE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER PROJECT INDEX]");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        if (index == null || index < 0) throw new EmptyIndexException();
        getProjectTaskService().removeByIndex(userId, index);
        System.out.println("[OK]");
    }

}
