package ru.tsc.pavlov.tm.util;

public interface StringUtil {

    static boolean isEmpty(final String value){
        if (value == null || value.isEmpty()) return true;
        else return false;
    }

    static boolean isEmpty(final String [] value) {
        if (value == null || value.length == 0) return true;
        else return false;
    }

}
