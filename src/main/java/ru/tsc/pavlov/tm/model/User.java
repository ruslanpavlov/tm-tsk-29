package ru.tsc.pavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.util.HashUtil;

import java.util.UUID;

@Getter
@Setter
public class User extends AbstractEntity {

    @NotNull private String id = UUID.randomUUID().toString();

    @NotNull private String login;

    @NotNull private String passwordHash;

    @Nullable private String email;

    @NotNull private UserRole role = UserRole.USER;

    @Nullable private String firstName;

    @Nullable private String lastName;

    @Nullable private String middleName;

    private Boolean locked = false;

    public User() {
    }

    public User(@NotNull String login, @NotNull String password) {
        this.login = login;
        this.passwordHash = password;
    }

    public User(@NotNull String login, @NotNull String password, @NotNull UserRole userRole) {
        this.login = login;
        this.passwordHash = password;
        this.role = userRole;
    }

    @Override
    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @NotNull
    public String getPassword() {
        return passwordHash;
    }

    public void setPassword(@NotNull String password) {
        this.passwordHash = password;
    }

    @Nullable public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    @NotNull public UserRole getRole() {
        return role;
    }

    public void setRole(@NotNull UserRole role) {
        this.role = role;
    }

    @Nullable public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Nullable public String getLastName() {
        return lastName;
    }

    public void setLastName(@Nullable String lastName) {
        this.lastName = lastName;
    }

    @Nullable public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(@Nullable String middleName) {
        this.middleName = middleName;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @Override
    public String toString() {
        return id + ": " + login;
    }

}
