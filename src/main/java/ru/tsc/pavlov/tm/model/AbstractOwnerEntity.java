package ru.tsc.pavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AbstractOwnerEntity extends AbstractEntity{

    @Nullable
    private String userId;

    @Nullable
    private String name;

    @NotNull
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date startDate;

    @Nullable
    protected Date finishDate;

    @NotNull
    protected Date created = new Date();

    @Override
    public String toString() {
        return id + ": " + name;
    }


}
