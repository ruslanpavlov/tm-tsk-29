package ru.tsc.pavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArg();

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getCommandByArg(@Nullable String name);

    void add(@NotNull AbstractCommand command);

}
