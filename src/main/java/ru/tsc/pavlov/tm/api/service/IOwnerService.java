package ru.tsc.pavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.IService;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService <E extends AbstractOwnerEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator);

    @NotNull
    E findById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    void remove(@NotNull String userId, @Nullable E entity);

    @NotNull
    Integer getSize(@NotNull String userId);

}
