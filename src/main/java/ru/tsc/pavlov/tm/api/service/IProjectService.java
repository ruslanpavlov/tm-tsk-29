package ru.tsc.pavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Project;

public interface IProjectService extends IOwnerService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable final String id, @Nullable final String name, @NotNull final String index);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description);

    boolean existsByIndex(@Nullable String userId, @Nullable int index);

    @NotNull
    Project startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
