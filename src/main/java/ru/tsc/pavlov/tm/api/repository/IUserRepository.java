package ru.tsc.pavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.IRepository;
import ru.tsc.pavlov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User removeByLogin(@NotNull String login);

}
